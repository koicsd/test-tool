function [runtime, vectNr, prodNr, impl] = cmpImpl(varargin)
%   cmpImpl("name1", data1, "name2", data2..., [bc,] [plotfun,] Nt)
%
%   Compares results of different measurements.
%   The datapoint containing Nt timesteps are collected
%   and plotted on one graph.

    % default values
    impl_names = string.empty;
    impl_data = cell.empty;
    
    getTbl = @(st) st.com;
    plotfun = @loglog;
    Nt2Find = NaN;
    
    % parsing args
    n = 1;
    while n <= nargin
        arg = varargin{n};
        if (ischar(arg) && isvector(arg)) || (isstring(arg) && isscalar(arg))
            if startsWith(arg, ".")
                getTbl = tblGetter(extractAfter(arg, "."));
            elseif strcmp(arg, "-linear")
                plotfun = @plot;
            elseif strcmp(arg, "-loglog")
                plotfun = @loglog;
            elseif strcmp(arg, "-semilogx")
                plotfun = @semilogx;
            elseif strcmp(arg, "-semilogy")
                plotfun = @semilogy;
            elseif strcmp(arg, "-errorbar")
                plotfun = @errorbar;
            elseif strcmp(arg, "-errorbarlog")
                plotfun = @errorbarlog;
            elseif strcmp(arg, "-errorbarlogx")
                plotfun = @errorbarlogx;
            elseif strcmp(arg, "-errorbarlogy")
                plotfun = @errorbarlogy;
            elseif strcmp(arg, "-pcolor")
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            elseif startsWith(arg, "-")
                error("Unknown flag: %s", arg);
            else  % name of implementation
                if n + 1 > nargin || ~iscell(varargin{n+1})
                    error("Name of implementation must be followed by a cell-array, containing data for all dimensions.")
                end
                impl_names(end+1) = convertCharsToStrings(arg);
                impl_data{end+1} = varargin{n+1};
                n = n + 1;
            end
        elseif isnumeric(arg)  % must be Nt
            if ~isnan(Nt2Find)
                error("Nt can be specified only once!")
            end
            if ~isscalar(arg) || mod(arg, 1) ~= 0
                error("Parameter Nt must be a scalar integer!")
            end
            Nt2Find = arg;
        elseif isa(arg, 'function_handle')  % either 'plotfun' or 'bc'
            if isequal(arg, @plot) || isequal(arg, @loglog) || isequal(arg, @semilogx) || isequal(arg, @semilogy)...
                    || isequal(arg, @errorbar) || isequal(arg, @errorbarlog) || isequal(arg, @errorbarlogx) || isequal(arg, @errorbarlogy)
                plotfun = arg;
            elseif isequal(arg, @pcolor)
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            else
                getTbl = arg;
            end
        else
            error("Wrong type of input no. %d", n);
        end
        n = n + 1;
    end
    
    if isnan(Nt2Find)
        error("Nt not specified!")
    end
    
    % doing stuff
    [impl, ~, runtime, vectNr, prodNr] = EvalUtil.collectRuntimeCrossimpl(getTbl, Nt2Find, impl_data{:});
    doPlot(plotfun, impl_names, runtime, vectNr, prodNr, impl, Nt2Find);
end

function getTbl = tblGetter(bcname)
    getTbl = @(st) st.(bcname);
end

function doPlot(plotfun, impl_names, runtime, vectNr, prodNr, impl_inds, Nt)
    if isempty(runtime)
        error('Empty plot!')
    end

    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '+xo';
    LINES = [struct('style', '-', 'width', 0.6)
        struct('style', '--', 'width', 1)
        struct('style', ':', 'width', 1.5)];
    
    hold off
    legends = string.empty;
    for implind = unique(impl_inds)  % implementation/algorythm
        dimind = 0;
        for n = find(impl_inds == implind)  % dim for implementation/algorythm
            dimind = dimind + 1;
            vectNr_ = vectNr{n};
            prodNr_ = prodNr{n};
            runtime_ = runtime{n};
            clr = COLORS(mod(implind, numel(COLORS))+1);
            mrk = MARKERS(mod(dimind, numel(MARKERS))+1);
            line = LINES(mod(implind-dimind, numel(LINES))+1);
            if isequal(plotfun, @errorbar) || isequal(plotfun, @errorbarlog) || isequal(plotfun, @errorbarlogx) || isequal(plotfun, @errorbarlogy)
                plotfun(prodNr_, runtime_.avg, runtime_.dev, [clr line.style mrk], 'LineWidth', line.width)
            else
                plotfun(prodNr_, runtime_.avg, [clr line.style mrk], 'LineWidth', line.width)
            end
            legends(end+1) = sprintf("%s, %dD, Nt=%d", impl_names(implind), size(vectNr_, 1), Nt);
            hold on
        end
    end
    hold off
    legend(legends, 'Location', 'Best');
    xlabel("N_r")
    ylabel("t_C_P_U [s]")
    title("Comparing implementations (only CPU time)")
end