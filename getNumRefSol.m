function refsol = getNumRefSol(Rinit, Rfin, Uinit, condscr, Tinit, Tfin, bc, varargin)
    global REF_DIR
    global CALC_REF
    filename = fullfile(REF_DIR, sprintf("%s.mat", strjoin([
        "Ref"
        sprintf("Grid%s", strjoin(string(size(Uinit)), "x"))
        string(condscr)
        sprintf("Coef%s", strjoin(string([varargin{:}]), ".."))
        string(bc)
        sprintf("T%s..%s", num2str(Tinit), num2str(Tfin))
    ], "_")));
    if exist(filename, 'file') == 2
        fprintf("Loading %s...", filename)
        filecontent = load(filename, '-mat', 'Rinit', 'Rfin', 'refsol');
        if isequal(filecontent.Rinit, Rinit) && isequal(filecontent.Rfin, Rfin)
            refsol = filecontent.refsol;
            fprintf(" Success.\n")
            return;
        else
            warning("Wrong scaling in file!\n")
        end
    end
    fprintf("Using reference algorythm...")
    refsol = CALC_REF(Uinit, Rinit, Rfin, Tinit, Tfin, bc, varargin{:});
    save(filename, 'Rinit', 'Rfin', 'refsol')
    fprintf(" Saved to: %s\n", filename)
end
