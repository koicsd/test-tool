function [err, runtime, vectNr, dr, dt, Nt, impl_ind] = cmpErrRuntime(varargin)


    % default values
    impl_names = string.empty;
    impl_data = cell.empty;
    getTbl = @(st) st.com;
    plotfun = @loglog;
    names2filter = cell.empty;
    values2filter = cell.empty;
    
    % parsing args
    n = 1;
    while n <= nargin
        arg = varargin{n};
        if (ischar(arg) && isvector(arg)) || (isstring(arg) && isscalar(arg))
            if startsWith(arg, ".")
                getTbl = tblGetter(extractAfter(arg, "."));
            elseif strcmp(arg, "-linear")
                plotfun = @plot;
            elseif strcmp(arg, "-loglog")
                plotfun = @loglog;
            elseif strcmp(arg, "-semilogx")
                plotfun = @semilogx;
            elseif strcmp(arg, "-semilogy")
                plotfun = @semilogy;
            elseif strcmp(arg, "-errorbar")
                plotfun = @errorbar;
            elseif strcmp(arg, "-errorbarlog")
                plotfun = @errorbarlog;
            elseif strcmp(arg, "-errorbarlogx")
                plotfun = @errorbarlogx;
            elseif strcmp(arg, "-errorbarlogy")
                plotfun = @errorbarlogy;
            elseif strcmp(arg, "-pcolor")
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            elseif startsWith(arg, "-")
                error("Unknown flag: %s", arg);
            else  % name of implementation
                if n + 1 > nargin || ~isstruct(varargin{n+1})
                    error("Name of implementation must be followed by a structure, containing data for a certain number of dimensions.")
                end
                impl_names(end+1) = convertCharsToStrings(arg);
                impl_data{end+1} = varargin{n+1};
                [names2filter{end+1}, values2filter{end+1}, n] = parseFilterKeys(varargin, n+1, getTbl(impl_data{end}));
            end
        elseif isa(arg, 'function_handle')  % either 'plotfun' or 'bc'
            if isequal(arg, @plot) || isequal(arg, @loglog) || isequal(arg, @semilogx) || isequal(arg, @semilogy) ...
                    || isequal(arg, @errorbar) || isequal(arg, @errorbarlog) || isequal(arg, @errorbarlogx) || isequal(arg, @errorbarlogy)
                plotfun = arg;
            elseif isequal(arg, @pcolor)
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            else
                getTbl = arg;
            end
        else
            error("Wrong type of input no. %d", n);
        end
        n = n + 1;
    end
    
    ERRNAME = struct('max', "Err_M_a_x", 'rms', "Err_R_M_S");
    
    % doing stuff
    %[impl, Nt, dt, runtime, err, dr, vectNr] = EvalUtil.collectErrRuntimeCrossimpl(getTbl, args2pass{:});
    [impl_ind, Nt, dt, runtime, err, dr, vectNr, ...
        drName, drUnit, vectNrName, NrFormat, lgdFormat] = ...
        collectData(getTbl, impl_data, names2filter, values2filter);
    doPlot(plotfun, impl_names, impl_ind, Nt, dt, runtime, err, dr, vectNr, ...
        ERRNAME.(EvalUtil.errtype), drName, drUnit, vectNrName, NrFormat, lgdFormat);
end

function getTbl = tblGetter(bcname)
    getTbl = @(st) st.(bcname);
end

function [names, values, n] = parseFilterKeys(args, n0, tbl)
    names = string.empty;
    values = cell.empty;
    n = n0;
    n0 = n0 + 1;
    if n0 > numel(args)
        return;
    end
    arg = args{n0};
    while (n0 <= numel(args) - 1) && ((ischar(arg) && isvector(arg)) ...
                                    || (isstring(arg) && isscalar(arg))) && ismember(arg, [fieldnames(tbl); {'Nr'}])
        names(end+1) = convertCharsToStrings(arg);
        if strcmp(names(end), "Nr")
            if ~isa(args{n0+1}, 'double') || ~ ismatrix(args{n0+1}) || size(args{n0+1}, 1) > 3
                error("Value for filter '%s' must be a double matrix with at most 3 rows!", names(end))
            end
        else
            if ~isa(args{n0+1}, 'double') || ~ isvector(args{n0+1})
                error("Value for filter '%s' must be a double vector!", names(end))
            end
        end
        values{end+1} = args{n0+1};
        n = n0 + 1;
        n0 = n0 + 2;
        if n0 > numel(args)
            return
        end
        arg = args{n0};
    end
end

function [impl_ind, Nt, dt, runtime, err, dr, vectNr, ...
        drName, drUnit, vectNrName, NrFormat, lgdFormat] = ...
        collectData(getTbl, impl_data, names2filter, values2filter)
    args2pass = [impl_data; names2filter; values2filter];
    [dim, impl_ind, Nt, dt, runtime, err, dr, vectNr] = EvalUtil.collectErrRuntimeCrossimpl(getTbl, args2pass{:});
    switch dim
        case 1
            drName = "dx";
            drUnit = "m";
            vectNrName = "N_x";
            NrFormat = @(vectNr_) sprintf("%d", vectNr_);
            %lgdFormat = @(impl_, dr_, vectNr_) sprintf("%s, dx=%.4e (Nx=%d)", impl_, dr_, vectNr_);
        case 2
            drName = "dA";
            drUnit = "m^2";
            vectNrName = "N_xxN_y";
            NrFormat = @(vectNr_) sprintf("%dx%d", vectNr_(1), vectNr_(2));
            %lgdFormat = @(impl_, dr_, vectNr_) sprintf("%s, dA=%.4e (%dx%d)", impl_, dr_, vectNr_(1), vectNr_(2));
        case 3
            drName = "dV";
            drUnit = "m^3";
            vectNrName = "N_xxN_yxN_z";
            NrFormat = @(vectNr_) sprintf("%dx%dx%d", vectNr_(1), vectNr_(2), vectNr_(3));
            %lgdFormat = @(impl_, dr_, vectNr_) sprintf("%s, dV=%.4e (%dx%dx%d)", impl_, dr_, vectNr_(1), vectNr_(2), vectNr_(3));
    end
    lgdFormat = @(impl_, dr_, vectNr_) sprintf("%s, %s=%.4e%s (%s=%s)", impl_, drName, dr_, drUnit, vectNrName, NrFormat(vectNr_));
end

function doPlot(plotfun, impl_names, impl_inds, Nt, dt, runtime, err, dr, vectNr, ...
        errname, drName, drUnit, vectNrName, NrFormat, lgdFormat)
    impl = impl_names(impl_inds);
    assert(isequal(size(runtime.avg), size(runtime.dev)))  % just to make sure everything is all right
    assert(isequal(size(runtime.avg), size(err)))
    assert(all(cellfun(@(ra,rd,e) isequal(size(ra), size(rd)) && isequal(size(ra), size(e)), runtime.avg, runtime.dev, err)))
    if isempty(runtime.avg) || all(cellfun(@(c) isempty(c), runtime.avg))
        error("Empty plot!")
    end
    
    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '*+xos^>v<d';
    LINES = [struct('style', '-', 'width', 0.6)
        struct('style', '--', 'width', 1)
        struct('style', ':', 'width', 1.5)];
    
    % plot data % many grids, many timescales
    legends = string.empty;
    hold off
    for implind = unique(impl_inds)  % implenetation/algorithm
        nr_ind = 0;
        for n = find(impl_inds == implind)  % Nr of implementation/algorithm
            nr_ind = nr_ind + 1;
            mrk = MARKERS(mod(nr_ind-1, numel(MARKERS))+1);
            clr = COLORS(mod(implind-1, numel(COLORS))+1);
            line = LINES(mod(implind-1, numel(LINES))+1);
            if isequal(plotfun, @errorbar) || isequal(plotfun, @errorbarlog) || isequal(plotfun, @errorbarlogx) || isequal(plotfun, @errorbarlogy)
                plotfun(runtime.avg{n}, err{n}, runtime.dev{n}, 'horizontal', [clr line.style mrk], 'LineWidth', line.width)
            else
                plotfun(runtime.avg{n}, err{n}, [clr line.style mrk], 'LineWidth', line.width);
            end
            legends(end+1) = lgdFormat(impl_names(implind), dr(n), vectNr(:,n));
            hold on
        end
    end
    hold off
    
    xlabel("t_C_P_U [s]")
    ylabel(errname)
    lgd = legend(legends, 'Location', 'northeastoutside');
    title("Comparing implementations (CPU time with error)")
    
    % add datacursor update-callback
    dcm = datacursormode;
    dcm.UpdateFcn = @(~, info) cursorUpdate(...
        info, impl, errname, "", ...
        dt, "s", Nt, ...
        dr, drName, drUnit, vectNr, vectNrName, NrFormat);
%     if exist('lgdiName', 'var') == 1
%         dcm.UpdateFcn = @(~, info) cursorUpdate(...
%             info, errname, Nt, dt, vectNr, formatNr, ...
%             dr, drName, drUnit, dpiName, lgdiName);
%     else
%         dcm.UpdateFcn = @(~, info) cursorUpdate(...
%             info, errname, Nt, dt, vectNr, formatNr, ...
%             dr, drName, drUnit, dpiName);
%     end
end

function text = cursorUpdate(info, impl, errname, errunit, ...
        dt, dtUnit, Nt, ...
        dr, drName, drUnit, vectNr, vectNrName, NrFormat)
    rt = info.Position(1);
    err = info.Position(2);
    Runtime = info.Target.XData;
    Error = info.Target.YData;
    [~, i] = min(abs(Runtime - rt));
    err = Error(i);
    j = info.Target.SeriesIndex;

    text = sprintf(...
        "%s\nCPU-time: %.4e s\n%s: %.4e %s\ndt: %.4e %s (Nt: %d)\n%s=%.4e%s (%s=%s)", ...
        impl(j), rt, errname, err, errunit, dt{j}(i), dtUnit, Nt{j}(i), drName, dr(j), drUnit, vectNrName, NrFormat(vectNr(:,j)));
end