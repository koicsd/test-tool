function [runtime, vectNr, prodNr, Nt] = cmpDims(varargin)
%   cmpDims([data,] [bc,] [plotfun,] Nt1[, Nt2...])
%
%   Compares CPU-time of different number of dimensions
%   for some given Nt timestep-counts.

    % parsing args
    getTbl = @(st) st.com;
    plotfun = @loglog;
    Nt2find = double.empty;
    for n = 1 : nargin
        arg = varargin{n};
        if iscell(arg)
            data = arg;
        elseif isa(arg, 'function_handle')  %% either 'plotfun' or 'bc'
            if isequal(arg, @pcolor)
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            elseif isequal(arg, @plot) || isequal(arg, @semilogx) || isequal(arg, @semilogy) || isequal(arg, @loglog)...
                    || isequal(arg, @errorbar) || isequal(arg, @errorbarlog) || isequal(arg, @errorbarlogx) || isequal(arg, @errorbarlogy)
                plotfun = arg;
            else
                getTbl = arg;
            end
        elseif (ischar(arg) && isvector(arg)) || (isstring(arg) && isscalar(arg))
            if strcmp(arg, "-linear")
                plotfun = @plot;
            elseif strcmp(arg, "-loglog")
                plotfun = @loglog;
            elseif strcmp(arg, "-semilogx")
                plotfun = @semilogx;
            elseif strcmp(arg, "-semilogy")
                plotfun = @semilogy;
            elseif strcmp(arg, "-errorbar")
                plotfun = @errorbar;
            elseif strcmp(arg, "-errorbarlog")
                plotfun = @errorbarlog;
            elseif strcmp(arg, "-errorbarlogx")
                plotfun = @errorbarlogx;
            elseif strcmp(arg, "-errorbarlogy")
                plotfun = @errorbarlogy;
            elseif strcmp(arg, "-pcolor")
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            elseif startsWith(arg, ".")
                getTbl = tblGetter(extractAfter(arg, 1));
            elseif startsWith(arg, "-")
                error('Invalid flag: %s', arg);
            else
                data = convertCharsToStrings(arg);
            end
        elseif isa(arg, 'double')
            if ~isvector(arg)
                error("Nt must be a (double) vector or scalar!")
            end
            if iscolumn(arg)
                arg = arg';
            end
            Nt2find = [Nt2find arg];
        end
    end

    % load CSV if necessary
    if exist('data', 'var') == 0
        data = readbackCSV;
    end
    if isstring(data)
        data = readbackCSV(data);
    end
    
    % do stuff
    [~, Nt, runtime, vectNr, prodNr] = EvalUtil.collectRuntimeCrossimpl(getTbl, Nt2find, data);
    doPlot(plotfun, Nt2find, runtime, vectNr, prodNr, Nt);
end

function getTbl = tblGetter(bcname)
    getTbl = @(st) st.(bcname);
end

function doPlot(plotfun, Nt2find, runtime, vectNr, prodNr, Nt)
    if isempty(runtime)
        error('Empty plot!')
    end

    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '+xo';
    LINES = [struct('style', '-', 'width', 0.6)
        struct('style', '--', 'width', 1)
        struct('style', ':', 'width', 1.5)];
    
    hold off
    legends = string.empty;
    Nt_ind = 0;
    for Nt_ = unique(Nt2find)  % Nt
        Nt_ind = Nt_ind + 1;
        dimind = 0;
        for n = find(Nt==Nt_)  % dim for given Nt
            dimind = dimind + 1;
            vectNr_ = vectNr{n};
            prodNr_ = prodNr{n};
            runtime_ = runtime{n};
            clr = COLORS(mod(Nt_ind, numel(COLORS))+1);
            mrk = MARKERS(mod(dimind, numel(MARKERS))+1);
            line = LINES(mod(Nt_ind-dimind, numel(LINES))+1);
            if isequal(plotfun, @errorbar) || isequal(plotfun, @errorbarlog) || isequal(plotfun, @errorbarlogx) || isequal(plotfun, @errorbarlogy)
                plotfun(prodNr_, runtime_.avg, runtime_.dev, [clr line.style mrk], 'LineWidth', line.width)
            else
                plotfun(prodNr_, runtime_.avg, [clr line.style mrk], 'LineWidth', line.width);
            end
            legends(end+1) = sprintf("%dD, Nt=%d", size(vectNr_, 1), Nt(n));
            hold on
        end
    end
    hold off
    legend(legends, 'Location', 'Best');
    xlabel("N_r")
    ylabel("t_C_P_U [s]")
    title("Comparing dimensions")
end