function errorbarlog(varargin)
%ERRORBARLOG The same as @errorbar but sets scales logarithmic
%   Internally invokes @errorbar itself, then sets scales
errorbar(varargin{:})
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
end