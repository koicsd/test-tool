% Initializing test framework.
% Connect containing directory to path.
% Copy this file to your local working directory and
% fill the placeholders to reference your custom functions.

GEN_INIT  % First, initialize reference generator

global FILE_NAMES  % path to store initial and result files
FILE_NAMES = struct(...  for communication via CLI, where datafile is necessary
    'Init', "init.grd",...
    'FixBC', "fixedbc.grd",...
    'Period', "periodic.grd",...
    'Isol', "isolated.grd"...
);

global GRID2FILE  % function to store initial data to file
GRID2FILE = @none;
% GRID2FILE = @WriteGrid;
% function WriteGrid(filename, data, Rinit, Rfin)

global READBACK  % function to read back numeric result from file
READBACK = @none;
%     READBACK = @ReadGrid;
% function [data, Rinit, Rfin] = ReadGrid(filename)

global FUNC2TEST  % actual function to be tested
FUNC2TEST = @myFunc2test;
% function Ufin = myFunc2test(Uinit, Rinit, Rfin, Nt, Tinit, Tfin, bc, coeffs...)  % if GRID2FILE and READBACK is specified
% function myFunc2test(initfile, Nt, Tinit, Tfin, resultfile, bc, coeffs...)  % if GRID2FILE and READBACK is not specified

% Base directory to store CSV files
% collector = DataCollector(csvDir, showPopup);
collector = DataCollector("csv", true);
% Param1: path to directory
% Param2: true to always display directory-open popup, as a confirmation