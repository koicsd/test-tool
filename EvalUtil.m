classdef EvalUtil
    %EvalUtil
    %   This is a static-method-only utility class to help dataprocessing

    methods (Access=private)
        function obj = EvalUtil()
        end
    end

    methods (Static)
        function et = errtype(val)
            %EvalUtil.errype
            %   A static function to set and query 
            %   the error-type (max or rms) to be collected.
            persistent errtype_;
            if nargin
                val = convertStringsToChars(val);
                if ~strcmp(val, 'max') && ~strcmp(val, 'rms')
                    error("Invalid type of error: %s", val);
                end
                errtype_ = val;
            end
            if isempty(errtype_)
                errtype_ = 'max';
            end
            et = errtype_;
        end
        
        function Nr_vect = vectNr(tbl)
            Nr_vect = [tbl.Nx];
            if isfield(tbl, 'Ny')
                Nr_vect = [Nr_vect; [tbl.Ny]];
            end
            if isfield(tbl, 'Nz')
                Nr_vect = [Nr_vect; [tbl.Nz]];
            end
        end
        
        function dr_prod = dr(tbl)
            if isfield(tbl, 'dV')
                dr_prod = [tbl.dV];
            elseif isfield(tbl, 'dA')
                dr_prod = [tbl.dA];
            else
                dr_prod = [tbl.dx];
            end
        end

        function [tbl, dim] = preproc_dim(tbl)
            % [tbl, dim] = EvalUtil.preproc_dim(tbl) 
            %   This function decides the dimension of data in the table,
            %   and adds field Nr and dA or dV to the datatable
            if isfield(tbl, 'Nz')
                dim = 3;
                assert(isfield(tbl, 'dV'), '3D data structure must contain field ''dV''')
                Nr = num2cell(cellfun(@(nx, ny, nz) nx*ny*nz, {tbl.Nx}, {tbl.Ny}, {tbl.Nz}));
                [tbl.Nr] = Nr{:};
            elseif isfield(tbl, 'Ny')
                dim = 2;
                assert(isfield(tbl, 'dA'), '2D data structure must contain field ''dA''')
                Nr = num2cell(cellfun(@(nx, ny) nx*ny, {tbl.Nx}, {tbl.Ny}));
                [tbl.Nr] = Nr{:};
            elseif isfield(tbl, 'Nx')
                dim = 1;
                Nr = {tbl.Nx};
                [tbl.Nr] = Nr{:};
            else  % terrible big bug
                dim = 0;
            end
        end
        
        function filtered = filter(tbl, names, values)
            %filtered = EvalUtil.filter(tbl, names, values)
            %   Filter data in table. Param 'names' must be a string vector
            %   or a cell vector of char vectors, whereas 'values' must be 
            %   a cell vector of vectors of the desired values. Param
            %   'names' and 'values' must have the same size.
            if nargin < 3  % let's suppose, there is no real aim to filter
                names = string.empty;
                values = cell.empty;
            end
            if isempty(names)
                filtered = tbl;
                return
            end
            names = convertCharsToStrings(names);
            if ~isstring(names) || ~iscell(values) || ~isvector(names) || ~isvector(values) || any(size(names) ~= size(values))
                error('Param ''name'' and ''values'' must be a vector of the same size. Param ''name'' must be interpetable as a string vector, whereas ''values'' must be a cell vector.');
            end
            filtered = tbl;
            for i = numel(names)
                name = names(i);
                vals = values{i};
                if ~strcmp(name, "Nr") && ~isvector(vals)
                    error("Each element of 'values' must be a vector of desired values, unless the appropriate 'name' element is ""Nr""!")
                end
                if ~ismatrix(vals)
                    error("Each element of 'values' must be a matrix, even if the appropriate 'name' element is ""Nr""!")
                end
                if strcmp(name, "Nr") && ~isvector(vals)
                    filtered = filtered(ismember(EvalUtil.vectNr(filtered)', vals', 'rows')');
                else
                    filtered = filtered(ismember([filtered.(name)], vals));
                end
            end
        end

        function [Nt, runtime, Nr_vect, Nr_prod] = collectRuntime(tbl, ivals, jvals)
            if nargin < 3
                jvals = double.empty;
            end
            if nargin < 2
                ivals = double.empty;
            end
            
            % collect unique Nr and Nt values
            Nt = unique([tbl.Nt]);
            Nr_vect = unique(EvalUtil.vectNr(tbl)', 'rows')';
            [Nr_prod, j] = sort(prod(Nr_vect, 1));
            Nr_vect = Nr_vect(:,j);
            
            % filter for index
            if ~isempty(ivals)
                Nt = Nt(ivals);
            end
            if ~isempty(jvals)
                Nr_vect = Nr_vect(:,jvals);
                Nr_prod = Nr_prod(jvals);
            end
            
            % collect runtime data for Nr and Nt values to a matrix
            runtime = struct;
            runtime.avg = NaN(numel(Nt), numel(Nr_prod));
            runtime.dev = NaN(numel(Nt), numel(Nr_prod));
            for record = tbl'
                i = Nt==record.Nt;
                j = all(Nr_vect==EvalUtil.vectNr(record), 1);
                runtime.avg(i,j) = record.runtime.avg;
                runtime.dev(i,j) = record.runtime.dev;
            end
        end

        function [dt, err, dr_prod, Nr_vect] = collectError(tbl, ivals, jvals)
            if nargin < 3
                jvals = double.empty;
            end
            if nargin < 2
                ivals = double.empty;
            end
            
            % collect unique 'dr' and 'dt'
            % actually: as 'dy' and 'dz' is not stored, using 'Nr' instead
            dt = unique([tbl.dt]);
            dr_prod = EvalUtil.dr(tbl);
            [Nr_vect, j, ~] = unique(EvalUtil.vectNr(tbl)', 'rows');
            Nr_vect = Nr_vect';
            dr_prod = dr_prod(j);
            [dr_prod, j] = sort(dr_prod);
            Nr_vect = Nr_vect(:,j);
            
            % filter for index
            if ~isempty(ivals)
                dt = dt(ivals);
            end
            if ~isempty(jvals)
                Nr_vect = Nr_vect(:,jvals);
                dr_prod = dr_prod(jvals);
            end
            
            % collect runtime data for 'dr' and 'dt' values to a matrix
            err = NaN(numel(dt), numel(dr_prod));
            for record = tbl'
                i = dt==record.dt;
                j = dr_prod==EvalUtil.dr(record);
                err(i,j) = record.err.(EvalUtil.errtype);
            end
        end

        function [runtime, Nt, dt, err, dr_prod, Nr_vect] = collectErrRuntime(tbl, ivals, jvals)
            if nargin < 3
                jvals = double.empty;
            end
            if nargin < 2
                ivals = double.empty;
            end
            
            % collect unique [dt; Nt] and [dr; vectNr] col vectors
            Nt = unique([[tbl.dt];[tbl.Nt]]', 'rows')';
            dt = Nt(1,:);
            Nt = Nt(2,:);
            Nr_vect = unique([EvalUtil.dr(tbl);EvalUtil.vectNr(tbl)]', 'rows')';
            dr_prod = Nr_vect(1,:);
            Nr_vect = Nr_vect(2:end,:);

            % filter for index
            if ~isempty(ivals)
                dt = dt(ivals);
                Nt = Nt(ivals);
            end
            if ~isempty(jvals)
                dr_prod = dr_prod(jvals);
                Nr_vect = Nr_vect(:,jvals);
            end


            % collect corresponding 'runtime' and 'err' vector to a matrix
            runtime = struct;
            runtime.avg = NaN(numel(dt), numel(dr_prod));
            runtime.dev = NaN(numel(dt), numel(dr_prod));
            err = NaN(numel(dt), numel(dr_prod));
            for record = tbl'
                i = dt==record.dt;
                j = all(Nr_vect==EvalUtil.vectNr(record), 1);
                runtime.avg(i,j) = record.runtime.avg;
                runtime.dev(i,j) = record.runtime.dev;
                err(i,j) = record.err.(EvalUtil.errtype);
            end
        end
        
        function [implID, Nt, runtime, Nr_vect, Nr_prod] = collectRuntimeCrossimpl(getTbl, Nt2find, varargin)
            runtime = {};
            Nr_vect = {};
            Nr_prod = {};
            Nt = [];
            implID = [];
            for n = 1 : numel(varargin)
                impldata = varargin{n};
                for dim_ = 1: numel(impldata)
                    if isempty(fieldnames(impldata{dim_}))
                        continue
                    end
                    tbl = EvalUtil.filter(EvalUtil.preproc_dim(getTbl(impldata{dim_})), "Nt", {Nt2find});
                    [Nt_, runtime_, Nr_vect_, Nr_prod_] = EvalUtil.collectRuntime(tbl);
                    implID = [implID repmat(n, 1, numel(Nt_))];
                    Nt = [Nt Nt_];
                    runtime = [runtime num2cell(runtime_, 2)'];
                    Nr_vect = [Nr_vect repmat({Nr_vect_}, 1, numel(Nt_))];
                    Nr_prod = [Nr_prod repmat({Nr_prod_}, 1, numel(Nt_))];
                end
            end
        end
        
        function [dim, implID, Nt, dt, runtime, err, dr_prod, Nr_vect] = collectErrRuntimeCrossimpl(getTbl, varargin)
        % collectRuntimeCrossimpl(bc, data1, [names1, vals1,] data2...)
            runtime = struct;
            runtime.avg = {};
            runtime.dev = {};
            Nt = {};
            dt = {};
            err = {};
            Nr_vect = [];
            dr_prod = [];
            implID = [];
            impl_ = 0;
            dim = 0;
            for n = 1 : numel(varargin)
                arg = varargin{n};
                if isa(arg, 'struct')
                    impldata = arg;
                    names = string.empty;
                    values = cell.empty;
                    impl_ = impl_ + 1;
                elseif isstring(arg)
                    names = arg;
                elseif iscell(arg)
                    values = arg;
                else
                    error("Each datastructure must be followed by the next one, or optionally names (string array) and values (cell array of numeric vectors) to filter!");
                end
                if n < numel(varargin) && ~isa(varargin{n+1}, 'struct')
                    continue
                end
                if isempty(fieldnames(impldata))
                    continue
                end
                [tbl, dim_] = EvalUtil.preproc_dim(getTbl(impldata));
                if dim ~= 0 && dim ~= dim_
                    error("Data to be compared must have the same number of dimensions!")
                end
                dim = dim_;
                tbl = EvalUtil.filter(tbl, names, values);
                [runtime_, Nt_, dt_, err_, dr_prod_, Nr_vect_] = EvalUtil.collectErrRuntime(tbl);
%                 implID = [implID repmat(impl_, 1, size(Nt_, 2))];
%                 runtime = [runtime num2cell(runtime_, 2)'];
%                 Nt = [Nt Nt_];
%                 dt = [dt, dt_];
%                 err = [err, num2cell(err_, 2)'];
%                 dr_prod = [dr_prod repmat({dr_prod_}, 1, numel(Nt_))];
%                 Nr_vect = [Nr_vect repmat({Nr_vect_}, 1, numel(Nt_))];
                implID = [implID repmat(impl_, 1, numel(dr_prod_))];
                dr_prod = [dr_prod dr_prod_];
                Nr_vect = [Nr_vect Nr_vect_];
                runtime.avg = [runtime.avg num2cell(runtime_.avg', 2)'];
                runtime.dev = [runtime.dev num2cell(runtime_.dev', 2)'];
                err = [err, num2cell(err_', 2)'];
                dt = [dt repmat({dt_}, 1, numel(dr_prod_))];
                Nt = [Nt repmat({Nt_}, 1, numel(dr_prod_))];
            end
        end
    end
end