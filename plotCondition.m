function plotCondition(st, varargin)
    GROUPTITLES = ["Runtime", "Error", "Error -- runtime function"];
    PLOTFUNS = {@plotRuntime, @plotErr, @plotErrRuntime};
    
    args2pass = {};
    for n = 1 : numel(varargin)
        arg = varargin{n};
        if (ischar(arg) && isrow(arg)) || (isstring(arg) && isscalar(arg))
            if startsWith(arg, "-")
                args2pass{end+1} = arg;
            else
                subt = arg;
            end
        elseif isa(arg, 'function_handle')
            if any(cellfun(@(fptr) isequal(fptr, arg), PLOTFUNS))
                plotfun = arg;
            else
                args2pass{end+1} = arg;
            end
        end
    end
    if exist('subt', 'var') == 0
        subt = "";
    end
    
    if exist('plotfun', 'var') == 0
        for n = 1 : numel(PLOTFUNS)
            plotfun = PLOTFUNS{n};
            figure
            plotCondition(st, plotfun, subt, args2pass{:})
        end
        return;
    end
    
    
    tbls = {'com', 'FixBC', 'Period', 'Isol'};
    titles = ["Common", "Dirichlet", "Periodic", "Neumann"];
    for n = 1 : numel(tbls)
        tbl = tbls{n};
        tit = titles(n);
        if ~isfield(st, tbl) || isempty(st.(tbl))
            continue
        end
        subplot(2,2, n)
        plotfun(st.(tbl), args2pass{:});
        title(tit)
    end
    grptit = GROUPTITLES(cellfun(@(fptr) isequal(fptr, plotfun), PLOTFUNS));
    if strlength(subt) > 0
        sgtitle(sprintf("%s (%s)", grptit, subt))
    else
        sgtitle(grptit)
    end
end