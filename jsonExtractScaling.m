function jsonExtractScaling(jsonName, scriptDir)
    scaling = loadScalingJSON(jsonName);
    exception = MException("jsonExtractScaling:BatchException", ...
        "An error occurred while extracting scaling from file:\n%s", absolutepath(jsonName));
    if isfield(scaling, 'Timescales')
        exception = fsCreateTimescales(exception, scriptDir, 'Timescales', scaling.Timescales, 0);
    end
    for dim = 1:3
        tslistname = sprintf('Timescales%dD', dim);
        gridlistname = sprintf('Grids%dD', dim);
        if isfield(scaling, tslistname)
            exception = fsCreateTimescales(exception, scriptDir, tslistname, scaling.(tslistname), dim);
        end
        if isfield(scaling, gridlistname)
            exception = fsCreateGrids(exception, scriptDir, gridlistname, scaling.(gridlistname), dim);
        end
    end
    if ~isempty(exception.cause)
        throw(exception)
    end
end

function scaling = loadScalingJSON(filename)
    [fid, errmsg] = fopen(filename);
    if fid == -1
        error("jsonExtractScaling:FileException", ...
            "Unable to open file for reading: %s\nReason: %s", ...
            absolutepath(filename), errmsg)
    end
    cleanupObj = onCleanup(@() fclose(fid));
    str = char(fread(fid, inf)');
    try
        scaling = jsondecode(str);
    catch cause
        myExc = MException("jsonExtractScaling:SyntaxError", ...
            "Error parsing file:\n%s", filename);
        myExc = addCause(myExc, cause);
        throw(myExc)
    end
end

function exception = fsCreateTimescales(exception, scriptDir, listName, arr, dim)
    if isempty(arr)
        return
    end
    if iscell(arr)
        for n = 1 : numel(arr)
            try
                fsCreateTimescale(scriptDir, listName, n, arr{n}, dim)
            catch cause
                exception = addCause(exception, cause);
            end
        end
    else  % isstruct(arr)
        for n = 1 : numel(arr)
            try
                fsCreateTimescale(scriptDir, listName, n, arr(n), dim)
            catch cause
                exception = addCause(exception, cause);
            end
        end
    end
end

function exception = fsCreateGrids(exception, scriptDir, listName, arr, dim)
    if isempty(arr)
        return
    end
    if iscell(arr)
        for n = 1 : numel(arr)
            try
                fsCreateGrid(scriptDir, listName, n, arr{n}, dim)
            catch cause
                exception = addCause(exception, cause);
            end
        end
    else  % isstruct(arr)
        for n = 1 : numel(arr)
            try
                fsCreateGrid(scriptDir, listName, n, arr(n), dim)
            catch cause
                exception = addCause(exception, cause);
            end
        end
    end
end

function fsCreateTimescale(scriptDir, listName, n, st, dim)
    if ~isfield(st, 'Tinit') || ~isfield(st, 'Tfin')
        error("jsonExtractScaling:SemanticError", ...
            "Entry %s(%d) must have both Tinit and Tfin value!", ...
            listName, n)
    end
    st.T = st.Tfin - st.Tinit;
    if isfield(st, 'Nt')
        if st.Nt <= 0 || mod(st.Nt, 1) ~= 0
            error("jsonExtractScaling:SemanticError", ...
                "%s(%d).Nt value must be a positive integer!", ...
                listName, n)
        end
        st.dt = st.T / st.Nt;
    elseif ~isfield(st, 'dt')
        error("jsonExtractScaling:SemanticError", ...
            "Entry %s(%d) must have either Nt or dt value!", ...
            listName, n)
    else
        if st.dt <= 0
            error("jsonExtractScaling:SemanticError", ...
                "%s(%d).dt value must be positive!", ...
                listName, n)
        end
        st.Nt = round(st.T / st.dt);
        st.dt = st.T / st.Nt;
    end
    switch dim
        case 0
            scriptName = sprintf("Time%d.m", st.Nt);
        case 1
            scriptName = sprintf("Time%d_1D.m", st.Nt);
        case 2
            scriptName = sprintf("Time%d_2D.m", st.Nt);
        case 3
            scriptName = sprintf("Time%d_3D.m", st.Nt);
    end
    writelines([
        sprintf("Tinit=%s; Tfin=%s; T=Tfin-Tinit;", string([st.Tinit st.Tfin]))
        sprintf("Nt=%d; dt=T/Nt;", st.Nt)
    ], fullfile(scriptDir, scriptName));
end

function fsCreateGrid(scriptDir, listName, n, st, dim)
    if ~isfield(st, 'Rinit') || ~isfield(st, 'Rfin')
        error("jsonExtractScaling:SemanticError", ...
            "Entry %s(%d) must have both Rinit and Rfin vector!", ...
            listName, n)
    end
    if ~isvector(st.Rinit) || numel(st.Rinit) ~= dim
        error("jsonExtractScaling:SemanticError", ...
            "Entry %s(%d).Rinit must be a vector with %d elements!", ...
            listName, n, dim)
    end
    if ~isvector(st.Rfin) || numel(st.Rfin) ~= dim
        error("jsonExtractScaling:SemanticError", ...
            "Entry %s(%d).Rfin must be a vector with %d elements!", ...
            listName, n, dim)
    end
    st.R = st.Rfin - st.Rinit;
    if isfield(st, 'Nr')
        if ~isvector(st.Nr) || numel(st.Nr) ~= dim
            error("jsonExtractScaling:SemanticError",...
                "Entry %s(%d).Nr must be a vector with %d elements!", ...
                listName, n, dim)
        end
        if any(st.Nr < 2) || any(mod(st.Nr, 1) ~= 0)
            error("jsonExtractScaling:SemanticError", ...
                "All elements of vector %s(%d).Nr must be an integer of value at least 2!", ...
                listName, n)
        end
        st.dr = st.R ./ (st.Nr - 1);
    elseif ~isfield(st, 'dr')
        error("jsonExtractScaling:SemanticError", ...
            "Entry %s(%d) must have either Nr or dr vector!", ...
            listName, n)
    else
        if ~isvector(st.dr) || numel(st.dr) ~= dim
            error("jsonExtractScaling:SemanticError", ...
                "Entry %s(%d).dr must be a vector with %d elements!", ...
                listName, n, dim)
        end
        if any(st.dr <= 0)
            error("jsonExtractScaling:SemanticError", ...
                "All elements of vector %s(%d).dr must be positive!", ...
                listName, n)
        end
        st.Nr = round(st.R ./ st.dr) + 1;
        st.dr = st.R ./ (st.Nr - 1);
    end
    switch dim
        case 1
            scriptName = sprintf("Grid%d.m", st.Nr);
            writelines([
                sprintf("Xinit=%s; Xfin=%s; X=Xfin-Xinit;", string([st.Rinit, st.Rfin]))
                sprintf("Nx=%d; dx=X/(Nx-1);", st.Nr)
                "x=Xinit:dx:Xfin;"
            ], fullfile(scriptDir, scriptName));
        case 2
            scriptName = sprintf("Grid%dx%d.m", st.Nr(1), st.Nr(2));
            writelines([
                sprintf("Xinit=%s; Xfin=%s; X=Xfin-Xinit;", string([st.Rinit(1) st.Rfin(1)]))
                sprintf("Yinit=%s; Yfin=%s; Y=Yfin-Yinit;", string([st.Rinit(2), st.Rfin(2)]))
                sprintf("Nx=%d; dx=X/(Nx-1);", st.Nr(1))
                sprintf("Ny=%d; dy=Y/(Ny-1);", st.Nr(2))
                "x=Xinit:dx:Xfin;"
                "y=Yinit:dy:Yfin;"
                "[XX, YY] = meshgrid(x, y);"
            ], fullfile(scriptDir, scriptName));
        case 3
            scriptName = sprintf("Grid%dx%dx%d.m", st.Nr(1), st.Nr(2), st.Nr(3));
            writelines([
                sprintf("Xinit=%s; Xfin=%s; X=Xfin-Xinit;", string([st.Rinit(1) st.Rfin(1)]))
                sprintf("Yinit=%s; Yfin=%s; Y=Yfin-Yinit;", string([st.Rinit(2) st.Rfin(2)]))
                sprintf("Zinit=%s; Zfin=%s; Z=Zfin-Zinit;", string([st.Rinit(3), st.Rfin(3)]))
                sprintf("Nx=%d; dx=X/(Nx-1);", st.Nr(1))
                sprintf("Ny=%d; dy=Y/(Ny-1);", st.Nr(2))
                sprintf("Nz=%d; dy=Z/(Nz-1);", st.Nr(3))
                "x=Xinit:dx:Xfin;"
                "y=Yinit:dy:Yfin;"
                "z=Zinit:dz:Zfin;"
                "[XX, YY, ZZ] = meshgrid(x, y, z);"
            ], fullfile(scriptDir, scriptName))
    end
end

function writelines(lines, filename)
    [fid, errmsg] = fopen(filename, 'wt');
    if fid == -1
        error("jsonExtractScaling:FileError", ...
            "Unable to open file for writing: %s\nReason: %s", ...
            absolutepath(filename), errmsg)
    end
    cleanupobj = onCleanup(@() fclose(fid));
    for line = lines
        fprintf(fid, "%s\n", line);
    end
end