global GRID2FILE
global FILE_NAMES

if endsWith(condscr, "_1D")

    plot(x, U.Init, ':r', 'DisplayName', 'Initial')
    xlabel('x'), ylabel('value'), legend('-dynamicLegend', 'Location', 'best')

    if isa(GRID2FILE, 'function_handle') && ~isequal(GRID2FILE, @none)
        fprintf("Writing initial data to file...\n")
        GRID2FILE(FILE_NAMES.Init, U.Init, Xinit, Xfin)
        fprintf("Data written to: %s\n", absolutepath(FILE_NAMES.Init))
    end

elseif endsWith(condscr, "_2D")

    figure(1)
    image([Xinit, Xfin], [Yinit, Yfin], U.Init, 'CDataMapping', 'scaled')
    % surf(X, Y, U.init)
    colorbar; lim = caxis; title('Initial')
    
    if isa(GRID2FILE, 'function_handle') && ~isequal(GRID2FILE, @none)
        fprintf("Writing initial data to file...\n")
        GRID2FILE(FILE_NAMES.Init, U.Init, [Xinit Yinit], [Xfin Yfin])
        fprintf("Data written to: %s\n", absolutepath(FILE_NAMES.Init))
    end

else
    error('Number of dimensions cannot be recognized!')
end

