function M = rms(X, opt)
if isvector(X) || (nargin >= 2 && strcmp(opt, 'all'))
    M = sqrt(sum(X.^2, 'all') / numel(X));
elseif ismatrix(X)
    M = sqrt(sum(X.^2) / size(X, 1));
end
