function [runtime, Nt, vectNr] = plotRuntime(tbl, varargin)
    names = string.empty;
    values = cell.empty;
    ivals = double.empty;
    jvals = double.empty;
    plotfun = @loglog;
    % process varargin
    n = 1;
    while n <= numel(varargin)
        arg = varargin{n};
        if isa(arg, 'function_handle')
            plotfun = arg;
        elseif (ischar(arg) && isrow(arg)) || (isstring(arg) && isscalar(arg))
            if ismember(arg, [fieldnames(tbl); {'Nr'}])
                names(end+1) = convertCharsToStrings(arg);
                values{end+1} = varargin{n+1};
                n = n + 1;
            elseif strcmp(arg, 'i')
                n = n + 1;
                arg = varargin{n};
                if ~isnumeric(arg) || any(arrayfun(@(e) e <= 0 || mod(e, 1) > 0, arg))
                    error('Values of ''i'' must be positive integers!')
                end
                ivals = arg;
            elseif strcmp(arg, 'j')
                n = n + 1;
                arg = varargin{n};
                if ~isnumeric(arg) || any(arrayfun(@(e) e <= 0 || mod(e, 1) > 0, arg))
                    error('Values of ''j'' must be positive integers!')
                end
                jvals = arg;
            elseif strcmp(arg, "-linear")
                plotfun = @plot;
            elseif strcmp(arg, "-loglog")
                plotfun = @loglog;
            elseif strcmp(arg, "-semilogx")
                plotfun = @semilogx;
            elseif strcmp(arg, "-semilogy")
                plotfun = @semilogy;
            elseif strcmp(arg, "-errorbar")
                plotfun = @errorbar;
            elseif strcmp(arg, "-errorbarlog")
                plotfun = @errorbarlog;
            elseif strcmp(arg, "-errorbarlogx")
                plotfun = @errorbarlogx;
            elseif strcmp(arg, "-errorbarlogy")
                plotfun = @errorbarlogy;
            elseif strcmp(arg, "-pcolor")
                plotfun = @pcolor;
            elseif startsWith(arg, "-")
                error('Invalid flag: %s', arg)
            else
                subt = convertCharsToStrings(arg);
            end
        end
        n = n + 1;
    end
    
    % do stuff
    [tbl, nrDispName, lgdFormatNr] = preprocDim(tbl);
    tbl = EvalUtil.filter(tbl, names, values);
    [Nt, runtime, vectNr, prodNr] = EvalUtil.collectRuntime(tbl, ivals, jvals);
    if isequal(plotfun, @pcolor)
        doPlotPcolor(runtime.avg, Nt, prodNr, nrDispName)
    elseif isequal(plotfun, @errorbar) || isequal(plotfun, @errorbarlog) || isequal(plotfun, @errorbarlogx) || isequal(plotfun, @errorbarlogy)
        doPlotErrbar(plotfun, runtime, Nt, prodNr, nrDispName, ...
            lgdFormatNr, vectNr)
    else
        doPlot(plotfun, runtime.avg, Nt, prodNr, nrDispName, ...
            lgdFormatNr, vectNr)
    end
    title("CPU time [s]")
    if exist('subt', 'var') == 1
        subtitle(subt)
    end
end

function [tbl, nrDispName, lgdFormatNr] = preprocDim(tbl)
    [tbl, dim] = EvalUtil.preproc_dim(tbl);
    switch dim
        case 3
        nrDispName = "N_r";
        lgdFormatNr = @(Nr) ...
            sprintf("N_xxN_yxN_z=%dx%dx%d", Nr(1), Nr(2), Nr(3));
        case 2
        nrDispName = "N_r";
        lgdFormatNr = @(Nr) sprintf("N_xxN_y=%dx%d", Nr(1), Nr(2));
        case 1
        nrDispName = "N_x";
        lgdFormatNr = @(Nr) sprintf("N_x=%d", Nr);
    end
end

function doPlot(plotfun, runtime, Nt, prodNr, nrDispName, ...
        lgdFormatNr, vectNr)
    if isempty(runtime)
        error("Empty plot!")
    end
    
    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '*+xosd';
    
    % plot data
    legends = repmat(string, 0);
    if numel(Nt) == 1
        plotfun(prodNr, runtime, 'bo-');
        plotXName = nrDispName;
        dpiName = "j";
        legends(end+1) = sprintf("N_t=%d", Nt);
    elseif numel(prodNr) == 1
        plotfun(Nt, runtime', 'bo-');
        plotXName = "N_t";
        dpiName = "i";
        legends(end+1) = lgdFormatNr(vectNr);
    else
        hold off
        for n = 1 : numel(prodNr)
            mrk = MARKERS(mod(n-1, numel(MARKERS))+1);
            clr = COLORS(mod(n-1, numel(COLORS))+1);
            %filtered = tbl([tbl.Nr]==Nr(n));
            %plotfun(Nt, arrayfun(@(rec) rec.runtime.avg, filtered)', 'o-');
            plotfun(Nt, runtime(:,n)', [clr mrk '-']);
            legends(end+1) = lgdFormatNr(vectNr(:,n));
            hold on
        end
        hold off
        plotXName = "N_t";
        dpiName = "i";
        lgdiName = "j";
    end
    xlabel(plotXName)
    ylabel("t_C_P_U [s]")
    lgd = legend(legends, 'Location', 'Best');
    
    % add datacursor update-callback
    dcm = datacursormode;
    if exist('lgdiName', 'var') == 1
        dcm.UpdateFcn = @(~, info) ...
            cursorUpdate(info, plotXName, lgd, dpiName, lgdiName);
    else
        dcm.UpdateFcn = @(~, info) ...
        cursorUpdate(info, plotXName, lgd, dpiName);
    end
end

function doPlotErrbar(plotfun, runtime, Nt, prodNr, nrDispName, ...
        lgdFormatNr, vectNr)
    if isempty(runtime)
        error("Empty plot!")
    end
    
    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '*+xosd';
    
    % plot data
    legends = repmat(string, 0);
    if numel(Nt) == 1
        plotfun(prodNr, runtime.avg, runtime.dev, 'bo-');
        plotXName = nrDispName;
        dpiName = "j";
        legends(end+1) = sprintf("N_t=%d", Nt);
    elseif numel(prodNr) == 1
        plotfun(Nt, runtime.avg', runtime.dev, 'bo-');
        plotXName = "N_t";
        dpiName = "i";
        legends(end+1) = lgdFormatNr(vectNr);
    else
        hold off
        for n = 1 : numel(prodNr)
            mrk = MARKERS(mod(n-1, numel(MARKERS))+1);
            clr = COLORS(mod(n-1, numel(COLORS))+1);
            plotfun(Nt, runtime.avg(:,n)', runtime.dev(:,n)', [clr mrk '-']);
            legends(end+1) = lgdFormatNr(vectNr(:,n));
            hold on
        end
        hold off
        plotXName = "N_t";
        dpiName = "i";
        lgdiName = "j";
    end
    xlabel(plotXName)
    ylabel("t_C_P_U [s]")
    lgd = legend(legends, 'Location', 'Best');
    
    % add datacursor update-callback
    dcm = datacursormode;
    if exist('lgdiName', 'var') == 1
        dcm.UpdateFcn = @(~, info) ...
            cursorUpdate(info, plotXName, lgd, dpiName, lgdiName);
    else
        dcm.UpdateFcn = @(~, info) ...
        cursorUpdate(info, plotXName, lgd, dpiName);
    end
end

function doPlotPcolor(runtime, Nt, prodNr, nrDispName)
    if isempty(runtime) || isvector(runtime)
        error("Empty plot!")
    end

    % plot data
    pcolor(prodNr, Nt, runtime)
    shading interp
    clb = colorbar;
    xlabel(nrDispName); ylabel("N_t")
    ylabel(clb, "t_C_P_U [s]")
    
    % add datacursor update-callback
    dcm = datacursormode;
    dcm.UpdateFcn = @(~, info) ...
        cursorUpdatePcolor(info, nrDispName, "N_t");
end

function text = cursorUpdate(info, xName, lgd, dpiName, lgdiName)
    x = info.Position(1);
    y = info.Position(2);
    X = info.Target.XData;
    Y = info.Target.YData;
    [~, i] = min(abs(X - x));
    y = Y(i);
    if numel(lgd.PlotChildren) > 1
        j = info.Target.SeriesIndex;
        text = sprintf("%s: %d\nt_C_P_U: %f s\n(%s)\n(%s,%s): (%d,%d)", ...
            xName, x, y, lgd.String{j}, dpiName, lgdiName, i, j);
    else
        text = sprintf("%s: %d\nt_C_P_U: %f s\n%s: %d", ...
            xName, x, y, dpiName, i);
    end
end

function text = cursorUpdatePcolor(info, xName, yName)
    x = info.Position(1);
    y = info.Position(2);
    X = info.Target.XData;
    Y = info.Target.YData;
    data = info.Target.CData;
    [~, i] = min(abs(Y - y));
    [~, j] = min(abs(X - x));
    x = X(j);
    y = Y(i);
    text = sprintf("%s: %d\n%s: %d\nt_C_P_U: %f s\n(i,j): (%d,%d)", ...
        xName, x, yName, y, data(i,j), i, j);
end