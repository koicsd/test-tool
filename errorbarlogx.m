function errorbarlogx(varargin)
%ERRORBARLOGX The same as @errorbar but sets X-scale logarithmic
%   Internally invokes @errorbar itself, then sets scale
errorbar(varargin{:})
set(gca, 'XScale', 'log')
end