function data = readbackCSV(folder)
    if nargin < 1
        folder = uigetdir("csv");
        if folder == 0
            error("No CSV directory specified")
        end
    end

    for dim = 1:3
        if isfolder(fullfile(folder, sprintf("%dD", dim)))
            data{dim} = readstatistics(fullfile(folder, sprintf("%dD", dim)));
        else
            data{dim} = struct;
        end
    end
    while ~isempty(data)
        if ~isempty(fieldnames(data{end}))
            break
        end
        data = data(1:end-1);
    end
end

function data = readstatistics(folder)
    data = readfolder(folder);
    initconds = dir(folder);
    initconds = initconds([initconds.isdir]);
    initconds = arrayfun(@(st) string(st.name), initconds)';
    initconds = initconds(~strcmp(initconds, ".") & ~strcmp(initconds, "..") & ~startsWith(initconds, "_"));
    for cond = initconds
        data.(cond) = readfolder(fullfile(folder, cond));
    end
end

function data = readfolder(folder)
    data = struct;
    data.com = tblRead(fullfile(folder, "com.csv"));
    data.FixBC = tblRead(fullfile(folder, "FixBC.csv"));
    data.Period = tblRead(fullfile(folder, "Period.csv"));
    data.Isol = tblRead(fullfile(folder, "Isol.csv"));
end

function data = tblRead(file)
    tbl = readtable(file, 'VariableNamingRule', 'preserve');
    fields = tbl.Properties.VariableNames;
    for field = fields
        field = field{1};
        tbl.(field) = convertCharsToStrings(tbl.(field));
    end
    values = table2cell(tbl);
    data = recursiveBuildNestedStruct(fields, values);
    
end

function starr = recursiveBuildNestedStruct(fields, values)
    starr = repmat(struct, size(values, 1), 1);
    structuredfields = contains(fields, '.');
    simplefields = ~structuredfields;
    
    for f = find(simplefields)
        field = fields{f};
        [starr(:).(field)] = values{:,f};
    end
    
    fieldstruct = struct;
    valuestruct = cell(size(values, 1), 0);
    for f = find(structuredfields)
        field = fields{f};
        terms = split(field, '.');
        prefix = terms{1};
        afterprefix = strjoin(terms(2:end), '.');
        if ~isfield(fieldstruct, prefix)
            fieldstruct.(prefix) = {afterprefix};
            valuestruct.(prefix) = values(:,f);
        else
            fieldstruct.(prefix){end+1} = afterprefix;
            valuestruct.(prefix) = [valuestruct.(prefix) values(:,f)];
        end
    end
    
    for prefix = fieldnames(fieldstruct)'
        prefix = prefix{1};
        subfields = fieldstruct.(prefix);
        subvalues = valuestruct.(prefix);
        substarr = num2cell(recursiveBuildNestedStruct(subfields, subvalues));
        [starr(:).(prefix)] = substarr{:};
    end
end
