% Initializing numeric reference factory.
% Connect containing directory to path.
% Copy this file to your local working directory and
% fill the placeholders to reference your custom functions.

% Directory containing problem generator scripts
% generator = Generator(pathToScriptFolder, showPopup);
generator = Generator("param&cond", true);
% Param1: path to directory
% Param2: true to always display directory-open popup, as a confirmation

global REF_DIR  % directory to cache numeric reference
REF_DIR = "refsol";
if ~isfolder(REF_DIR)
    mkdir(REF_DIR)
end

global CALC_REF  % custom function to calculate numeric reference
CALC_REF = @myFunc2calcNumRef;
% function Ufin = myFunc2calcNumRef(Uinit, Rinit, Rfin, Nt, Tinit, Tfin, bc, coeffs...)