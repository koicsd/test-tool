function errorbarlogy(varargin)
%ERRORBARLOGY The same as @errorbar but sets Y-scale logarithmic
%   Internally invokes @errorbar itself, then sets scale
errorbar(varargin{:})
set(gca, 'YScale', 'log')
end