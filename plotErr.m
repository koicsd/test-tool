function [err, dt, dr, vectNr] = plotErr(tbl, varargin)
    names = string.empty;
    values = cell.empty;
    ivals = double.empty;
    jvals = double.empty;
    plotfun = @loglog;
    % process varargin
    n = 1;
    while n <= numel(varargin)
        arg = varargin{n};
        if isa(arg, 'function_handle')
            plotfun = arg;
        elseif (ischar(arg) && isrow(arg)) || (isstring(arg) && isscalar(arg))
            if ismember(arg, [fieldnames(tbl); {'Nr'}])
                names(end+1) = convertCharsToStrings(arg);
                values{end+1} = varargin{n+1};
                n = n + 1;
            elseif strcmp(arg, 'i')
                n = n + 1;
                arg = varargin{n};
                if ~isnumeric(arg) || any(arrayfun(@(e) e <= 0 || mod(e, 1) > 0, arg))
                    error('Values of ''i'' must be positive integers!')
                end
                ivals = arg;
            elseif strcmp(arg, 'j')
                n = n + 1;
                arg = varargin{n};
                if ~isnumeric(arg) || any(arrayfun(@(e) e <= 0 || mod(e, 1) > 0, arg))
                    error('Values of ''j'' must be positive integers!')
                end
                jvals = arg;
            elseif strcmp(arg, "-linear")
                plotfun = @plot;
            elseif strcmp(arg, "-loglog")
                plotfun = @loglog;
            elseif strcmp(arg, "-semilogx")
                plotfun = @semilogx;
            elseif strcmp(arg, "-semilogy")
                plotfun = @semilogy;
            elseif strcmp(arg, "-pcolor")
                plotfun = @pcolor;
            elseif startsWith(arg, "-")
                error('Invalid flag: %s', arg);
            else
                subt = convertCharsToStrings(arg);
            end
        end
        n = n + 1;
    end
    
    % do stuff
    TIT = struct('max', "Error (Max.)", 'rms', "Error (RMS)");
    ERRNAME = struct('max', "Err_M_a_x", 'rms', "Err_R_M_S");
    [tbl, drName, drUnit, lgdFormatNr] = preprocDim(tbl);
    tbl = EvalUtil.filter(tbl, names, values);
    [dt, err, dr, vectNr] = EvalUtil.collectError(tbl, ivals, jvals);
    if isequal(plotfun, @pcolor)
        doPlotPcolor(err, dt, dr, ERRNAME.(EvalUtil.errtype), drName, drUnit)
    else
        doPlot(plotfun, err, dt, dr, ERRNAME.(EvalUtil.errtype), drName, drUnit, ...
            lgdFormatNr, vectNr)
    end
    title(TIT.(EvalUtil.errtype))
    if exist('subt', 'var') == 1
        subtitle(subt)
    end
end

function [tbl, drName, drUnit, lgdFormatNr] = preprocDim(tbl)
    [tbl, dim] = EvalUtil.preproc_dim(tbl);
    switch dim
        case 3  % 3D
        drName = 'dV';
        drUnit = "m^3";
        lgdFormatNr = @(dr, Nr) ...
            sprintf("dV=%.4em^3 (%dx%dx%d)", dr, Nr(1), Nr(2), Nr(3));
        case 2  % 2D
        drName = 'dA';
        drUnit = "m^2";
        lgdFormatNr = @(dr, Nr) ...
            sprintf("dA=%.4em^2 (%dx%d)", dr, Nr(1), Nr(2));
        case 1  % 1D
        drName = 'dx';
        drUnit = "m";
        lgdFormatNr = @(dr, Nr) sprintf("dx=%.4em (Nx=%d)", dr, Nr);
    end
end

function doPlot(plotfun, err, dt, dr, errName, drName, drUnit, ...
        lgdFormatNr, vectNr)
    if isempty(err)
        error("Empty plot!")
    end
    
    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '*+xosd';
    
    % plot data
    legends = repmat(string, 0);
    if numel(dt) == 1
        plotfun(dr, err, 'bo-');
        plotXName = drName;
        plotXUnit = drUnit;
        dpiName = "j";
        legends(end+1) = sprintf("dt=%.4es", dt);
    elseif numel(dr) == 1
        plotfun(dt, err', 'bo-');
        plotXName = "dt";
        plotXUnit = "s";
        dpiName = "i";
        legends(end+1) = lgdFormatNr(dr, vectNr);
    else
        hold off
        for n = 1 : numel(dr)
            mrk = MARKERS(mod(n-1, numel(MARKERS))+1);
            clr = COLORS(mod(n-1, numel(COLORS))+1);
            plotfun(dt, err(:,n), [clr mrk '-']);
            legends(end+1) = lgdFormatNr(dr(n), vectNr(:,n));
            hold on
        end
        hold off
        plotXName = "dt";
        plotXUnit = "s";
        dpiName = "i";
        lgdiName = "j";
    end
    xlabel(sprintf("%s [%s]", plotXName, plotXUnit))
    ylabel(errName)
    lgd = legend(legends, 'Location', 'Best');
    
    
    % add datacursor update-callback
    dcm = datacursormode;
    if exist('lgdiName', 'var') == 1
        dcm.UpdateFcn = @(~, info) cursorUpdate(...
            info, plotXName, plotXUnit, errName, lgd, dpiName, lgdiName);
    else
        dcm.UpdateFcn = @(~, info) cursorUpdate(...
            info, plotXName, plotXUnit, errName, lgd, dpiName);
    end
end

function doPlotPcolor(err, dt, dr, errName, drName, drUnit)
    if isempty(err) || isvector(err)
        error("Empty plot")
    end
    
    % plot data
    pcolor(dr, dt, err)
    shading interp
    clb = colorbar;
    xlabel(sprintf("%s [%s]", drName, drUnit)); ylabel("dt [s]")
    ylabel(clb, errName)
    
    % add datacursor update-callback
    dcm = datacursormode;
    dcm.UpdateFcn = @(~, info) ...
        cursorUpdatePcolor(info, drName, "dt", drUnit, "s", errName);
end

function text = cursorUpdate(info, xName, xUnit, yName, lgd, ...
        dpiName, lgdiName)
    x = info.Position(1);
    y = info.Position(2);
    X = info.Target.XData;
    Y = info.Target.YData;
    [~, i] = min(abs(X - x));
    y = Y(i);
    if numel(lgd.PlotChildren) > 1
        j = info.Target.SeriesIndex;
        text = sprintf("%s: %e %s\n%s: %e\n(%s)\n(%s,%s): (%d,%d)", ...
            xName, x, xUnit, yName, y, lgd.String{j}, dpiName, lgdiName, i, j);
    else
        text = sprintf("%s: %e %s\n%s: %e\n%s: %d", ...
            xName, x, xUnit, yName, y, dpiName, i);
    end
end

function text = cursorUpdatePcolor(info, xName, yName, xUnit, yUnit, valName)
    x = info.Position(1);
    y = info.Position(2);
    X = info.Target.XData;
    Y = info.Target.YData;
    data = info.Target.CData;
    [~, i] = min(abs(Y - y));
    [~, j] = min(abs(X - x));
    x = X(j);
    y = Y(i);
    text = sprintf("%s: %e %s\n%s: %e %s\n%s: %e\n(i,j): (%d,%d)", ...
        xName, x, xUnit, yName, y, yUnit, valName, data(i,j), i, j);
end