global lastreftspan

if exist('ctrl_flags', 'var') == 0  % creating default value for ctrl_flags -- use this struct to omit some of the BCs
    ctrl_flags = struct('FixBC', true, 'Period', true, 'Isol', true);
end
if exist('getRefSol', 'var') == 0
    getRefSol = @getNumRefSol;  % by default, reference solution should be numeric
end
if ~all(isfield(U, fieldnames(ctrl_flags)))  % first run after condscr
    for bc = fieldnames(ctrl_flags)
        U.(bc{1}) = struct;    % let's init structures
    end
    lastreftspan = [NaN, NaN];  % let's create placeholder for tspan
end
if isequal(lastreftspan, [Tinit, Tfin])  % tspan not changed
    return  % unnecessary to run
end
disp('Getting reference solutions...')

[coeffs{1:nargout(@Coefficient)}] = Coefficient;
if endsWith(condscr, '_1D')
    
    COLORS = 'gcbmr';
    
    hold on
    n=0;
    for bc = convertCharsToStrings(fieldnames(ctrl_flags))'
        if ~ctrl_flags.(bc)
            continue
        end
        n = n + 1;
        clr = COLORS(mod(n-1, numel(COLORS))+1);
        fprintf(strcat("\t", bc, "...\n\t\t"))
        displayName = strcat(bc, " ref.");
        U.(bc).Ref = getRefSol(Xinit, Xfin, U.Init, condscr, Tinit, Tfin, bc, coeffs{:});  % !!!
        delete(findobj('type', 'line', 'DisplayName', displayName))
        plot(x, U.(bc).Ref, [clr '-'], 'DisplayName', displayName)
    end
    
elseif endsWith(condscr, '_2D')
    
    figure(1)
    if numel(findobj(gcf, 'type', 'Axes')) == 1  % Only one Axes, it must be the 'Initial'
        subplot(2,2, 1, gca);  % Rearrange graph to put 'Initial' to the top left corner
        sgtitle('Reference solutions')
    end
    plotid = 1;
    for bc = convertCharsToStrings(fieldnames(ctrl_flags))'
        plotid = plotid + 1;
        if ~ctrl_flags.(bc)
            continue
        end
        fprintf(strcat("\t", bc, "...\n\t\t"))
        U.(bc).Ref = getRefSol([Xinit Yinit], [Xfin Yfin], U.Init, condscr, Tinit, Tfin, bc, coeffs{:});
        subplot(2,2, plotid); image([Xinit Xfin], [Yinit Yfin], U.(bc).Ref, 'CDataMapping', 'scaled')
        if exist('axlim', 'var') == 1
            caxis(axlim);
        end
        colorbar; title(bc)
    end
    
else
    error("Number of dimensions cannot be recognized!")
end