classdef Generator < handle
    properties (SetAccess=immutable)
        COND_DIR
    end
    
    methods
        % Constructor to set COND_DIR and add it to path
        function obj = Generator(condDir, showDialog)
            if nargin < 1
                condDir = ".";
                showDialog = true;
            elseif nargin < 2
                showDialog = false;
            end
            if showDialog
                condDir = uigetdir(condDir, "Location of system generator scripts...");
                if condDir == 0
                    error("Location of system generator scripts not given!")
                end
            end
            obj.COND_DIR = convertCharsToStrings(condDir);
            if ~isstring(obj.COND_DIR) || ~isscalar(obj.COND_DIR)
                error("Script directory must be specified as a character vector or string scalar!")
            end
            addpath(obj.COND_DIR)
            jsonScaling = fullfile(obj.COND_DIR, "scaling.json");
            if exist(jsonScaling, 'file')
                jsonExtractScaling(jsonScaling, obj.COND_DIR);
            end
        end
        
        % Main function to get script names from COND_DIR
        function [grids, conditions, timescales] = getProblems(obj)
            grids = Generator.strGetGrids(obj.COND_DIR);
            conditions = Generator.strGetConditions(obj.COND_DIR);
            timescales = Generator.strGetTimescales(obj.COND_DIR);
        end
    end
    
    % Auxiliary functions to get script names from COND_DIR
    methods (Static=true, Access=private)
        function cell_grids = strGetGrids(COND_DIR)
            files = dir(fullfile(COND_DIR, "Grid*.m"));
            files = files(~[files.isdir]);
            grids = arrayfun(@Generator.fileInfo2scriptName, files)';
            grids1D = grids(count(extractAfter(grids, "Grid"), "x")==0);
            grids2D = grids(count(extractAfter(grids, "Grid"), "x")==1);
            grids3D = grids(count(extractAfter(grids, "Grid"), "x")==2);
            if ~isempty(grids3D)
                cell_grids = {grids1D; grids2D; grids3D};
            elseif ~isempty(grids2D)
                cell_grids = {grids1D; grids2D};
            elseif ~isempty(grids1D)
                cell_grids = {grids1D};
            else
                cell_grids = cell(0,1);
            end
        end

        function cell_conditions = strGetConditions(COND_DIR)
            files = [dir(fullfile(COND_DIR, "*_1D.m")); dir(fullfile(COND_DIR, "*_2D.m")); dir(fullfile(COND_DIR, "*_3D.m"))];
            files = files(~[files.isdir] & ~startsWith({files.name}, 'Time'));
            conditions = arrayfun(@Generator.fileInfo2scriptName, files)';
            conditions1D = conditions(endsWith(conditions, "_1D"));
            conditions2D = conditions(endsWith(conditions, "_2D"));
            conditions3D = conditions(endsWith(conditions, "_3D"));
            if ~isempty(conditions3D)
                cell_conditions = {conditions1D; conditions2D; conditions3D};
            elseif ~isempty(conditions2D)
                cell_conditions = {conditions1D; conditions2D};
            elseif ~isempty(conditions1D)
                cell_conditions = {conditions1D};
            else
                cell_conditions = cell(0,1);
            end
        end

        function cell_timescales = strGetTimescales(COND_DIR)
            files = dir(fullfile(COND_DIR, "Time*.m"));
            files = files(~[files.isdir]);
            timescales = arrayfun(@Generator.fileInfo2scriptName, files)';
            timescales1D = timescales(endsWith(timescales, "_1D") | ~contains(timescales, "_"));
            timescales2D = timescales(endsWith(timescales, "_2D") | ~contains(timescales, "_"));
            timescales3D = timescales(endsWith(timescales, "_3D") | ~contains(timescales, "_"));
            if ~isempty(timescales3D)
                cell_timescales = {timescales1D; timescales2D; timescales3D};
            elseif ~isempty(timescales2D)
                cell_timescales = {timescales1D; timescales2D};
            elseif ~isempty(timescales1D)
                cell_timescales = {timescales1D};
            else
                cell_timescales = cell(0,1);
            end
        end

        function str = fileInfo2scriptName(st)
            str = convertCharsToStrings(st.name);
            str = extractBefore(str, strlength(str) - strlength(".m") + 1);
        end
    end
    
    % Functions to execute generator scripts in a safe way
    methods
        function [Xinit, Xfin, X, Nx, dx, x, Yinit, Yfin, Y, Ny, dy, y, XX, YY] = matGetGrid(~, gridscr)
            Xinit = NaN; Xfin = NaN; X = NaN;
            Nx = 0; dx = NaN; x = [];
            Yinit = NaN; Yfin = NaN; Y = NaN;
            Ny = 0; dy = NaN; y = [];
            XX = []; YY = [];
            disp(strcat("Executing '", gridscr, "'..."))
            run(gridscr)
        end

        function [U, getRefSol, ctrl_flags] = matGetCondition(~, condscr, Xinit, Xfin, X, Nx, dx, x, Yinit, Yfin, Y, Ny, dy, y, XX, YY)
            U = struct('Init', [], 'FixBC', [], 'Period', [], 'Isol', []);
            getRefSol = @getNumRefSol;
            ctrl_flags = struct('FixBC', true, 'Period', true, 'Isol', true);
            disp(strcat("Executing '", condscr, "'..."))
            run(condscr)
        end

        function [Tinit, Tfin, T, Nt, dt, t] = matGetTimescale(~, timescr)
            Tinit = NaN; Tfin = NaN; T = NaN;
            Nt = 0; dt = NaN; t = [];
            disp(strcat("Executing '", timescr, "'..."))
            run(timescr)
        end
    end
    
    % Destructor to remove COND_DIR from path, as a cleanup
    methods (Access=private)
        function delete(obj)
            rmpath(obj.COND_DIR)
        end
    end
end