function data = Evaluate(data)
    if nargin < 1
        data = readbackCSV;
    elseif isstring(data) || (ischar(data) && isvector(data))
        data = readbackCSV(data);
    end
    
    close all
    for dim = 1:numel(data)
        statistics = data{dim};
        if isempty(fieldnames(statistics))
            continue
        end
        fprintf("Plotting %dD statistics...", dim)
        plotCondition(statistics, sprintf("%dD", dim))
        fprintf("OK\n")
    end
end


function plot_dimension(statistics, dim)
    for field = convertCharsToString(fieldnames(statistics))'
        if any(strcmp(field, ["com", "FixBC", "Period", "Isol"]))
            continue
        end
        plot_condition(sprintf(" (%s)", field), st, dim)
    end
    plot_condition("", statistics, dim)
end

function plot_condition(subtitle, st, dim)
    close all
    figure(1)  % runtime
    titles = ["Runtime", "Error (Max)", "Error (RMS)"];
    plotfuns = [@plot_runtime, @plot_ErrMax, @plot_ErrRMS];
    
    for figid = 1 : numel(plotfuns)
        fun = plotfuns(figid);
        sgtitle(sprintf("%s%s%dD", titles(figid), subtitle, dim))
        fun(st.com, "com", [2 2 1]);
        if isfield(st, "FixBC")
            fun(st.FixBC, "Fixed BC", [2 2 2])
        end
        if isfield(st, "Period")
            fun(st.Period, "Periodic", [2 2 3])
        end
        if isfield(st, "Isol")
            fun(st.Isol, "Isolated", [2 2 4])
        end
    end
end
