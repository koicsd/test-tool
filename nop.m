function [varargout] = nop(varargin)
    % nop()
    % A function for no-operation.
    % To be used as function handle
    % when there is no need to write initial data to file
    % or read back result from file.
    varargout = {};
end