function [runtime, Nt, dt, err, dr, vectNr] = plotErrRuntime(tbl, varargin)
    names = string.empty;
    values = cell.empty;
    ivals = double.empty;
    jvals = double.empty;
    plotfun = @loglog;
    % process varargin
    n = 1;
    while n <= numel(varargin)
        arg = varargin{n};
        if isa(arg, 'function_handle')
            plotfun = arg;
        elseif (ischar(arg) && isrow(arg)) || (isstring(arg) && isscalar(arg))
            if ismember(arg, [fieldnames(tbl); {'Nr'}])
                names(end+1) = convertCharsToStrings(arg);
                values{end+1} = varargin{n+1};
                n = n + 1;
            elseif strcmp(arg, 'i')
                n = n + 1;
                arg = varargin{n};
                if ~isnumeric(arg) || any(arrayfun(@(e) e <= 0 || mod(e, 1) > 0, arg))
                    error("Values of 'i' must be positive integers!")
                end
                ivals = arg;
            elseif strcmp(arg, 'j')
                n = n + 1;
                arg = varargin{n};
                if ~isnumeric(arg) || any(arrayfun(@(e) e <= 0 || mod(e, 1) > 0, arg))
                    error("Values of 'j' must be positive integers!")
                end
                jvals = arg;
            elseif strcmp(arg, "-linear")
                plotfun = @plot;
            elseif strcmp(arg, "-loglog")
                plotfun = @loglog;
            elseif strcmp(arg, "-semilogx")
                plotfun = @semilogx;
            elseif strcmp(arg, "-semilogy")
                plotfun = @semilogy;
            elseif strcmp(arg, "-errorbar")
                plotfun = @errorbar;
            elseif strcmp(arg, "-errorbarlog")
                plotfun = @errorbarlog;
            elseif strcmp(arg, "-errorbarlogx")
                plotfun = @errorbarlogx;
            elseif strcmp(arg, "-errorbarlogy")
                plotfun = @errorbarlogy;
            elseif strcmp(arg, "-pcolor")
                warning("@pcolor not allowed! Using @loglog instead.")
                plotfun = @loglog;
            elseif startsWith(arg, "-")
                error("Invalig flag: %s", arg);
            else
                subt = arg;
            end
        end
        n = n + 1;
    end
    if isequal(plotfun, @pcolor)
        warning("@pcolor not allowed! Using @loglog instead.")
        plotfun = @loglog;
    end
    
    % do stuff
    ERRNAME = struct('max', "Err_M_a_x", 'rms', "Err_R_M_S");
    [tbl, formatNr, lgdFormat, drName, drUnit] = preprocDim(tbl);
    tbl = EvalUtil.filter(tbl, names, values);
    [runtime, Nt, dt, err, dr, vectNr] = EvalUtil.collectErrRuntime(tbl, ivals, jvals);
    if isequal(plotfun, @errorbar) || isequal(plotfun, @errorbarlog) || isequal(plotfun, @errorbarlogx) || isequal(plotfun, @errorbarlogy)
        doPlotErrbar(plotfun, runtime, err, ERRNAME.(EvalUtil.errtype),...
            Nt, vectNr, formatNr, ...
            dt, dr, drName, drUnit, lgdFormat)
    else
        doPlot(plotfun, runtime.avg, err, ERRNAME.(EvalUtil.errtype),...
            Nt, vectNr, formatNr, ...
            dt, dr, drName, drUnit, lgdFormat)
    end
    title("Error -- CPU time function")
    if exist('subt', 'var') == 1
        subtitle(subt)
    end
end

function [tbl, formatNr, lgdFormat, drName, drUnit] = preprocDim(tbl)
    [tbl, dim] = EvalUtil.preproc_dim(tbl);
    switch dim
        case 3  % 3D
        formatNr = @(Nr) sprintf("N_xxN_yxN_z: %dx%dx%d", Nr(1), Nr(2), Nr(3));
        lgdFormat = @(dr, Nr) ...
            sprintf("dV=%.4em^3 (%dx%dx%d)", dr, Nr(1), Nr(2), Nr(3));
        drName = 'dV';
        drUnit = "m^3";
        case 2  % 2D
        formatNr = @(Nr) sprintf("N_xxN_y: %dx%d", Nr(1), Nr(2));
        lgdFormat = @(dr, Nr) ...
            sprintf("dA=%.4em^2 (%dx%d)", dr, Nr(1), Nr(2));
        drName = 'dA';
        drUnit = "m^2";
        case 1  % 1D
        formatNr = @(Nr) sprintf("N_x: %d", Nr);
        lgdFormat = @(dr, Nr) sprintf("dx=%.4em (Nx=%d)", dr, Nr);
        drName = 'dx';
        drUnit = "m";
    end
end

function doPlot(plotfun, runtime, err, errname,...
        Nt, vectNr, formatNr, ...
        dt, dr, drName, drUnit, lgdFormat)
    if isempty(runtime)
        error("Empty plot!")
    end
    
    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '*+xosd';
    
    % plot data
    legends = repmat(string, 0);
    if numel(dr) == 1  % only 1 grid
        plotfun(runtime, err, 'bo-')
        legends(end+1) = lgdFormat(dr, vectNr);
        dpiName = "i";
    elseif numel(dt) == 1  % only 1 timescale
        plotfun(runtime, err, 'bo-')
        legends(end+1) = sprintf("dt=%.4es", dt);
        dpiName = "j";
    else  %% many grids, many timescales
        hold off
        for n = 1 : size(runtime, 2)
            mrk = MARKERS(mod(n-1, numel(MARKERS))+1);
            clr = COLORS(mod(n-1, numel(COLORS))+1);
            plotfun(runtime(:,n)', err(:,n)', [clr mrk '-']);
            legends(end+1) = lgdFormat(dr(n), vectNr(:,n));
            hold on
        end
        hold off
        dpiName = "i";
        lgdiName = "j";
    end
    xlabel("t_C_P_U [s]")
    ylabel(errname)
    lgd = legend(legends, 'Location', 'northeastoutside');
    
    % add datacursor update-callback
    dcm = datacursormode;
    if exist('lgdiName', 'var') == 1
        dcm.UpdateFcn = @(~, info) cursorUpdate(...
            info, errname, Nt, dt, vectNr, formatNr, ...
            dr, drName, drUnit, dpiName, lgdiName);
    else
        dcm.UpdateFcn = @(~, info) cursorUpdate(...
            info, errname, Nt, dt, vectNr, formatNr, ...
            dr, drName, drUnit, dpiName);
    end
end

function doPlotErrbar(plotfun, runtime, err, errname,...
        Nt, vectNr, formatNr, ...
        dt, dr, drName, drUnit, lgdFormat)
    if isempty(runtime.avg)
        error("Empty plot!")
    end
    
    % line colors and marker styles for multiple plots
    COLORS = 'gcbmr';
    MARKERS = '*+xosd';
    
    % plot data
    legends = repmat(string, 0);
    if numel(dr) == 1  % only 1 grid
        plotfun(runtime.avg, err, runtime.dev, 'horizontal', 'bo-')
        legends(end+1) = lgdFormat(dr, vectNr);
        dpiName = "i";
    elseif numel(dt) == 1  % only 1 timescale
        plotfun(runtime.avg, err, runtime.dev, 'horizontal', 'bo-')
        legends(end+1) = sprintf("dt=%.4es", dt);
        dpiName = "j";
    else  %% many grids, many timescales
        hold off
        for n = 1 : size(runtime, 2)
            mrk = MARKERS(mod(n-1, numel(MARKERS))+1);
            clr = COLORS(mod(n-1, numel(COLORS))+1);
            plotfun(runtime.avg(:,n)', err(:,n)', runtime.dev(:,n)', 'horizontal', [clr mrk '-']);
            legends(end+1) = lgdFormat(dr(n), vectNr(:,n));
            hold on
        end
        hold off
        dpiName = "i";
        lgdiName = "j";
    end
    xlabel("t_C_P_U [s]")
    ylabel(errname)
    lgd = legend(legends, 'Location', 'northeastoutside');
    
    % add datacursor update-callback
    dcm = datacursormode;
    if exist('lgdiName', 'var') == 1
        dcm.UpdateFcn = @(~, info) cursorUpdate(...
            info, errname, Nt, dt, vectNr, formatNr, ...
            dr, drName, drUnit, dpiName, lgdiName);
    else
        dcm.UpdateFcn = @(~, info) cursorUpdate(...
            info, errname, Nt, dt, vectNr, formatNr, ...
            dr, drName, drUnit, dpiName);
    end
end

function text = cursorUpdate(info, yName, Nt, dt, vectNr, formatNr, ...
        dr, drName, drUnit, dpiName, lgdiName)
    x = info.Position(1);
    y = info.Position(2);
    X = info.Target.XData;
    Y = info.Target.YData;
    [~, i] = min(abs(X - x));
    y = Y(i);
    if exist('lgdiName', 'var') == 1
        j = info.Target.SeriesIndex;
        text = sprintf(...
            "t_C_P_U: %e s\n%s: %e\n(%s,%s): (%d,%d)\ndt: %.4e s\n%s: %.4e %s\nN_t: %d\n%s", ...
            x, yName, y, dpiName, lgdiName, i, j,...
            dt(i), drName, dr(j), drUnit,...
            Nt(i), formatNr(vectNr(:,j)));
    else
        text = sprintf(...
            "t_C_P_U: %e s\n%s: %e\n%s: %d\ndt: %.4e s\n%s: %.4e %s\nN_t: %d\n%s", ...
            x, yName, y, dpiName, i, ...
            dt, drName, dr, drUnit,...
            Nt, formatNr(vectNr));
    end
end
