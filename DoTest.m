RefSol

global FUNC2TEST
global READBACK
global FILE_NAMES
Runtime = struct(...
    'FixBC', 0,...
    'Period', 0,...
    'Isol', 0 ...
);
Error = struct(...
    'FixBC', struct('Max', NaN, 'Rms', NaN),...
    'Period', struct('Max', NaN, 'Rms', NaN),...
    'Isol', struct('Max', NaN, 'Rms', NaN)...
);

[coeffs{1:nargout(@Coefficient)}] = Coefficient;

if endsWith(condscr, "_1D")
    
    COLORS = 'gcbmr';
    MARKERS = '*+xosd';
    
    n = 0;
    for bc = fieldnames(ctrl_flags)'
        bc = bc{1};
        if ~ctrl_flags.(bc)
            continue;
        end
        n = n + 1;
        mrk = MARKERS(mod(n-1, numel(MARKERS))+1);
        clr = COLORS(mod(n-1, numel(COLORS))+1);
        disp([bc ':'])
        if isa(READBACK, 'function_handle') && ~isequal(READBACK, @none)
            tic
            FUNC2TEST(FILE_NAMES.Init, Nt, Tinit, Tfin, FILE_NAMES.(bc), bc, coeffs{:});
            Runtime.(bc) = toc;
            fprintf('Reading back result...\n');
            U.(bc).Res = READBACK(FILE_NAMES.(bc));
        else
            tic
            U.(bc).Res = FUNC2TEST(U.Init, Xinit, Xfin, Nt, Tinit, Tfin, bc, coeffs{:});
            Runtime.(bc) = toc;
        end
        fprintf('\tRuntime: %g sec\n', Runtime.(bc))
        delete(findobj('type', 'line', 'DisplayName', bc))
        plot(x, U.(bc).Res, [clr mrk], 'DisplayName', bc)
        Error.(bc).Max = max(abs(U.(bc).Res - U.(bc).Ref));
        Error.(bc).Rms = rms(U.(bc).Res - U.(bc).Ref);
        fprintf('\tError(Max): %g\n', Error.(bc).Max)
        fprintf('\tError(RMS): %g\n', Error.(bc).Rms)
    end
    
elseif endsWith(condscr, "_2D")
    
    figid = 1;
    for bc = fieldnames(ctrl_flags)'
        bc = bc{1};
        figid = figid + 1;
        if ~ctrl_flags.(bc)
            continue;
        end
        disp([bc ':'])
        if isa(READBACK, 'function_handle') && ~isequal(READBACK, @none)
            tic
            FUNC2TEST(FILE_NAMES.Init, Nt, Tinit, Tfin, FILE_NAMES.(bc), bc, coeffs{:});
            Runtime.(bc) = toc;
            fprintf('Reading back result...\n');
            [U.(bc).Res, ~, ~] = READBACK(FILE_NAMES.(bc));
        else
            tic
            U.(bc).Res = FUNC2TEST(U.Init, [Xinit Yinit], [Xfin Yfin], Nt, Tinit, Tfin, bc, coeffs{:});
            Runtime.(bc) = toc;
        end
        fprintf('\tRuntime: %g sec\n', Runtime.(bc))
        cpyrefplot(figid, bc)
        subplot(2,2, 4); image([Xinit Xfin], [Yinit Yfin], U.(bc).Res, 'CDataMapping', 'scaled'); colorbar; title('Calculated')
        if exist('axlim', 'var') == 1
            caxis(axlim);
        end
        subplot(2,2, 2); image([Xinit Xfin], [Yinit Yfin], U.(bc).Res - U.(bc).Ref, 'CDataMapping', 'scaled'); colorbar; title('Error')
        Error.(bc).Max = max(abs(reshape(U.(bc).Res - U.(bc).Ref, numel(U.Init), 1)));
        Error.(bc).Rms = rms(reshape(U.(bc).Res - U.(bc).Ref, numel(U.Init), 1));
        fprintf('\tError(Max): %g\n', Error.(bc).Max)
        fprintf('\tError(RMS): %g\n', Error.(bc).Rms)
    end
    
else
    error("Number of dimensions cannot be recognized!")
end

function cpyrefplot(figid, name)
    if ishandle(figid)
        figure(figid)
        return
    end
    figure(1)
    initax = subplot(2,2, 1);
    refax = subplot(2,2, figid);
    figure(figid); sgtitle(name)
    initax = copyobj(initax, gcf);
    subplot(2,2, 1, initax)
    colorbar(); title('Initial')
    refax = copyobj(refax, gcf);
    subplot(2,2, 3, refax)
    colorbar(); title('Reference')
end