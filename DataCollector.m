classdef DataCollector < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        CSV_DIR
    end
    
    properties(SetAccess=private)
        data
        DIM
    end
    
    methods
        function obj = DataCollector(csvDir, showDialog)
            if nargin < 1
                csvDir = ".";
                showDialog = true;
            elseif nargin < 2
                showDialog = false;
            end
            if showDialog
                csvDir = uigetdir(csvDir, "Directory to put CSV statistical files into...");
                if csvDir == 0
                    error("No directory selected to put CSV statistical files into!")
                end
            end
            obj.CSV_DIR = convertCharsToStrings(csvDir);
            if ~isstring(obj.CSV_DIR) || ~isscalar(obj.CSV_DIR)
                error("CSV directory must be specified as a character vector or string scalar!")
            end
            obj.data={};
        end
        
        function set.CSV_DIR(obj, csvDir)
            csvDir = convertCharsToStrings(csvDir);
            if ~isstring(csvDir) || ~isscalar(csvDir)
                error("CSV directory must be specified as a character vector or string scalar!")
            end
            obj.CSV_DIR = csvDir;
        end
        
        function obj = withData(obj, data)
            if ~iscell(data)
                error(strcat("Data must be given as a cell array,\n",...
                    "with each element denoting to differenctnumber of dimension!"))
            end
            obj.data = data;
        end
        
        function dim = get.DIM(obj)
            dim = numel(obj.data);
        end
        
        function loadAll(obj)
            obj.data = readbackCSV(obj.CSV_DIR);
        end
        
        function initDim(obj, dim, conditions)
            for d = (numel(obj.data) + 1) : dim
                obj.data{d} = struct;
            end
            
        end
        
        function insert(obj, dim, gridscr, condscr, timescr, ctrl_flags, ...
                Nt, dt, Nr, dr, runtime, err)
            
        end
        
        function saveDim(obj, dim)
            
        end
        
        function deleteDimDir(obj, dim)
            
        end
    end
end

